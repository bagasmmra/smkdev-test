# Node.js API SMKDEV-TEST

This is a Node.js API project with three main functionalities:
1. Find the highest palindrome by replacing up to `k` characters.
2. Check if brackets are balanced in a given string.
3. Calculate weights for substrings and characters and return results for the queries.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/bagasmmra/smkdev-test.git
   cd smkdev-test
   npm install
   npm run dev

API Documentation
Swagger is used for API documentation. You can access it at http://localhost:3000/api-docs.

