import { Request, Response } from 'express';
import { isBalanced } from '../services/balancedBracketsService';


/**
 * @swagger
 * /balanced-brackets:
 *   post:
 *     summary: Check if brackets are balanced
 *     description: Determine if the given string has balanced brackets.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               input:
 *                 type: string
 *                 example: "([{}])"
 *     responses:
 *       200:
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 input:
 *                   type: string
 *                   example: "([{}])"
 *                 result:
 *                   type: string
 *                   example: "YES"
 *       400:
 *         description: Invalid input
 */

export const checkBalancedBrackets = (req: Request, res: Response): void => {
    const { input } = req.body;

    if (typeof input !== 'string') {
        res.status(400).send('Invalid input');
        return;
    }

    const result = isBalanced(input);
    res.json({ input, result });
};
