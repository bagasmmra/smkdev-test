import { Request, Response } from 'express';
import { getWeights } from '../services/weightedStringsService';
/**
 * @swagger
 * /weighted-strings:
 *   post:
 *     summary: Get weighted strings
 *     description: Calculate weights for substrings and characters and return results for the queries.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               input:
 *                 type: string
 *                 example: "deeffggg"
 *               queries:
 *                 type: array
 *                 items:
 *                   type: integer
 *                 example: [5, 10, 21, 15]
 *     responses:
 *       200:
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 input:
 *                   type: string
 *                   example: "deeffggg"
 *                 queries:
 *                   type: array
 *                   items:
 *                     type: integer
 *                   example: [5, 10, 21, 15]
 *                 result:
 *                   type: array
 *                   items:
 *                     type: string
 *                   example: ["Yes", "Yes", "Yes", "No"]
 *       400:
 *         description: Invalid input
 */
export const getWeightedString = (req: Request, res: Response): void => {
    const { input, queries } = req.body;

    if (typeof input !== 'string' || !Array.isArray(queries)) {
        res.status(400).send('Invalid input');
        return;
    }

    const queryNumbers = queries.map(Number);
    const result = getWeights(input, queryNumbers);
    res.json({ input, queries: queryNumbers, result });
};
