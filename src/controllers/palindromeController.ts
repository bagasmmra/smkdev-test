import { Request, Response } from 'express';
import { highestPalindrome } from '../services/palindromeService';


/**
 * @swagger
 * /highest-palindrome:
 *   post:
 *     summary: Get the highest palindrome
 *     description: Find the highest palindrome by replacing up to k characters
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               str:
 *                 type: string
 *                 example: "12321"
 *               k:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       200:
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 result:
 *                   type: string
 *                   example: "13331"
 *       400:
 *         description: Invalid input
 */

export const getHighestPalindrome = (req: Request, res: Response): void => {
    const { str, k } = req.body;

    if (typeof str !== 'string' || typeof k !== 'number') {
        res.status(400).send('Invalid input');
        return;
    }

    const result = highestPalindrome(str, k);
    res.json({ result });
};
