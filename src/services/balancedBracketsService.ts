export const isBalanced = (input: string): string => {
    const stack: string[] = [];
    const bracketPairs: { [key: string]: string } = {
        ')': '(',
        '}': '{',
        ']': '['
    };

    for (const char of input) {
        if (['(', '{', '['].includes(char)) {
            stack.push(char);
        } else if ([')', '}', ']'].includes(char)) {
            if (stack.pop() !== bracketPairs[char]) {
                return 'NO';
            }
        }
    }

    return stack.length === 0 ? 'YES' : 'NO';
};
