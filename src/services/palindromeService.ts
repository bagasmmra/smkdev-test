export const highestPalindrome = (str: string, k: number): string => {
    const n = str.length;

    const makePalindrome = (s: string, l: number, r: number, k: number): string => {
        if (k < 0) return '-1';
        if (l >= r) return s;

        if (s[l] !== s[r]) {
            const maxChar = s[l] > s[r] ? s[l] : s[r];
            const left = replaceAt(s, l, maxChar);
            const right = replaceAt(left, r, maxChar);
            return makePalindrome(right, l + 1, r - 1, k - 1);
        } else {
            return makePalindrome(s, l + 1, r - 1, k);
        }
    };

    const makeHighest = (s: string, l: number, r: number, k: number): string => {
        if (k < 0) return '-1';
        if (l > r) return s;

        if (s[l] !== s[r]) {
            const maxChar = s[l] > s[r] ? s[l] : s[r];
            s = replaceAt(s, l, maxChar);
            s = replaceAt(s, r, maxChar);
            k--;
        }

        if (s[l] !== '9' && k > 0) {
            s = replaceAt(s, l, '9');
            s = replaceAt(s, r, '9');
            k--;
        }

        return makeHighest(s, l + 1, r - 1, k);
    };

    const replaceAt = (s: string, index: number, char: string): string => {
        return s.substr(0, index) + char + s.substr(index + 1);
    };

    const initialPalindrome = makePalindrome(str, 0, n - 1, k);
    if (initialPalindrome === '-1') return '-1';

    return makeHighest(initialPalindrome, 0, n - 1, k);
};
