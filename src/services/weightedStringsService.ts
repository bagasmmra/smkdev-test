export const getWeights = (input: string, queries: number[]): string[] => {
    const weightsSet = calculateWeights(input);
    return queries.map(query => (weightsSet.has(query) ? 'Yes' : 'No'));
};

export const calculateWeights = (input: string): Set<number> => {
    const weights = new Set<number>();

    const charWeight = (char: string): number => {
        return char.toLowerCase().charCodeAt(0) - 96;
    };

    let i = 0;
    while (i < input.length) {
        const char = input[i];
        const weight = charWeight(char);
        let sum = 0;

        for (let j = i; j < input.length && input[j] === char; j++) {
            sum += weight;
            weights.add(sum);
        }

        while (i < input.length && input[i] === char) {
            i++;
        }
    }

    return weights;
};



