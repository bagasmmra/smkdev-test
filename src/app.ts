import express, { Application } from 'express';
import { setupSwagger } from './swagger';
import indexRoutes from './routes/indexRoutes';

const app: Application = express();

app.use(express.json());
app.use('/api', indexRoutes);

setupSwagger(app);

export default app;
