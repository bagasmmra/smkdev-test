import { Router } from 'express';
import weightedStringsRouter from './weightedStringsRoutes';
import balancedBracketsRouter from './balancedBracketsRoutes';
import palindromeRoutes from './palindromeRoutes';

const router: Router = Router();

router.use('/weighted-strings', weightedStringsRouter);
router.use('/balanced-brackets', balancedBracketsRouter);
router.use('/highest-palindrome', palindromeRoutes);

export default router;
