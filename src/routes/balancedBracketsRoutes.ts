import { Router } from 'express';
import { checkBalancedBrackets } from '../controllers/balancedBracketsController';

const router: Router = Router();

router.post('/', checkBalancedBrackets);

export default router;
