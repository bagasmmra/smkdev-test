import { Router } from 'express';
import { getWeightedString } from '../controllers/weightedStringsController';

const router: Router = Router();

router.post('/', getWeightedString);

export default router;