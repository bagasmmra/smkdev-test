import { Router } from 'express';
import { getHighestPalindrome } from '../controllers/palindromeController';

const router: Router = Router();

router.post('/', getHighestPalindrome);

export default router;
